package com.game;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class Box {

	protected int width = 60;
	protected int height = 60;
	protected int margin = 10;
	protected int type = 2;
	protected int column;
	protected int row;
	
	public Box(int row, int column) 
	{
		// Set current row and column
		this.setRow(row);
		this.setColumn(column);
	}
	
	public Box()
	{
		return;
	}
	
	public int getColumn()
	{
		return this.column;
	}
	
	public void setColumn(int column)
	{
		this.column = column;
	}
	
	public int getRow()
	{
		return this.row;
	}
	
	public void setRow(int row)
	{
		this.row = row;
	}
	
	public int getX()
	{
		return ((this.getColumn() - 1) * this.width) + (this.getColumn() * this.margin) - 50;
	}
	
	public int getY()
	{
		return ((this.getRow() - 1) * this.width) + (this.getRow() * this.margin) - 50;
	}
	
	public void setType(int type)
	{
		this.type = type;
	}
	
	public int getType()
	{
		return this.type; 
	}
	
	public void setRandomType()
	{
		// Random set the type of box
		Random randomGenerator = new Random();
		this.type = randomGenerator.nextInt(4);
		this.type = this.type == 0 ? 1 : this.type;
	}
	
	public void draw(Graphics g)
	{
		if (this.type == 1) {
			g.setColor(Color.white);
		} else if (this.type == 2) {
			g.setColor(Color.PINK);
		} else if (this.type == 3) {
			g.setColor(Color.PINK);
		}
		g.fillRect( this.getX(), this.getY(), this.width, this.height);
	}
}
