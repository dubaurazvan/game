package com.game;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Main extends JPanel implements MouseListener, MouseMotionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Main();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		customPaint(g);
	}
	
	public void customPaint(Graphics g) {
		g.setFont(new Font("",0,16));
		
//		g.setColor(Color.LIGHT_GRAY);
//		g.fillRect( 0, 0, this.getWidth(), 400);
		
		g.setColor(Color.lightGray);
		g.fillRect( 20, 20, 290, 290);
		
		for (int i = 0; i < 36; i++) {
			BoxHandler.boxes[i].draw(g);
		}
		
//		g.setColor(Color.black);
//		for (int i = 1; i <= 4; i++) {
//			g.drawRect(BoxHandler.rectRow[i].getBounds().x, 
//						BoxHandler.rectRow[i].getBounds().y, 
//						BoxHandler.rectRow[i].getBounds().width, 
//						BoxHandler.rectRow[i].getBounds().height);
//		}
		
//		g.setColor(Color.blue);
//		for (int i = 0; i < 4; i++) {
//			g.drawRect(BoxHandler.rectColumn[i].getBounds().x, 
//						BoxHandler.rectColumn[i].getBounds().y, 
//						BoxHandler.rectColumn[i].getBounds().width, 
//						BoxHandler.rectColumn[i].getBounds().height);
//		}
	}
	
	public static JFrame myFrame;
	public Main() {
		//Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
		final JFrame myFrame = new JFrame("Puzzle Java Graphics");
		myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container myPane = myFrame.getContentPane();
		myPane.setLayout(null);
		
		myFrame.setBackground(Color.white);
		myFrame.setSize(470, 700);
		myFrame.setVisible(true);
		myFrame.setContentPane(this);
		myFrame.addMouseListener(this);
		myFrame.addMouseMotionListener(this);
		
		// Get box handler
		BoxHandler boxHandler = new BoxHandler();
		boxHandler.initialBoxes();
		
		run();
	}
	
	public void run() {
	   while (true) {
	      repaint();
	      try {
	         Thread.sleep(17);
	      } catch (InterruptedException e) {
	         e.printStackTrace();
	      }
	   }
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		//System.out.println("mouseEntered");
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		//System.out.println("mouseExited");
	}

	public static MouseEvent clickPoint = null;
	
	@Override
	public void mousePressed(MouseEvent e) {
		// Store the event to compare with the mouseReleased
		// and get the direction of move after click
		clickPoint = e;
		
		// Convert the click to match the play screen
		clickPoint = SwingUtilities.convertMouseEvent(myFrame, clickPoint, getParent());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	public void log(Object obj) {
		System.out.println(obj);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// Convert the click to match the play screen
		e = SwingUtilities.convertMouseEvent(myFrame, e, getParent());
		
		if (clickPoint != null) {
			
			int xDiff = e.getX() - clickPoint.getX();
			// xDiff = xDiff < 0 ?  xDiff * (-1) : xDiff;
			int yDiff = e.getY() - clickPoint.getY();
			// yDiff = yDiff < 0 ?  yDiff * (-1) : yDiff;
			
			// We have some consistent move after click
			if (xDiff > 40 || xDiff < -40 || yDiff > 40 || yDiff < -40) {
				
				String direction = null;
				// Get direction
				if (xDiff > 40) {
					direction = "right";
				} else if (xDiff < -40) {
					direction = "left";
				} else if (yDiff > 40) {
					direction = "down";
				} else if (yDiff < -40) {
					direction = "up";
				}
				
				//log("here");
				
				for (int i = 1; i <= 4; i++) {
					if (direction == "right" || direction == "left") {
						if (BoxHandler.rectRow[i].contains(clickPoint.getPoint())) {
							BoxHandler.moveLine("row", i, direction);
						}	
					} else {
						if (BoxHandler.rectColumn[i].contains(clickPoint.getPoint())) {
							log(direction + " -> " +  i);
							BoxHandler.moveLine("column", i, direction);
						}
					}
				}
				clickPoint = null;
			}
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (clickPoint instanceof MouseEvent) {
//			
			clickPoint = null;
		}
	}
	
}
