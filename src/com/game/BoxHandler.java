package com.game;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class BoxHandler {

	public static int perLine = 6;
	public static int boxCount = perLine * perLine;
	public static Rectangle[] rectRow = new Rectangle[5]; // will keep 0 unused
	public static Rectangle[] rectColumn = new Rectangle[5]; // will keep 0 unused
	public static Box[] boxes = new Box[boxCount];
	public static int L = 60;
	public static boolean inMove = false;
	
	/**
	 * Arrange the initial screen
	 * 
	 * return void
	 */
	public static void initialBoxes()
	{
		// Add initial boxes
		int i = 0;
		while (i < boxCount) {
			
			int row = (int) (Math.ceil((double) (i + 1) / perLine));
			int column = (int) ((i + 1) - Math.floor( (double) i / perLine ) * perLine);
			
			// TODO use some recursive method to arrange the boxes
//			1   _2___3___4___5__  6
//			7  | 8   9   10  11 | 12
//			13 | 14  15  16  17 | 18			
//			19 | 20  21  22  23 | 24
//			25 |_26__27__28__29_| 30
//			31   32  33  34  35   36
			
			boxes[i] = new Box(row, column);
			
			if (row > 1 && row < 6 && column > 1 && column < 6) {
				boxes[i].setType(1);
				if (i == 28) {
					boxes[i].setType(2);
				}
			} else {
				boxes[i].setRandomType();
			}
			
			i++;
		}
		
		addRowsRectangles();
		addColumnsRectangles();
	}
	
	/**
	 * This rectangles are used only for click events
	 * There will be 4 rectangles for each central rows
	 * 
	 * return void
	 */
	public static void addRowsRectangles()
	{
		for (int i = 1; i <= 4; i++) {
			rectRow[i] = new Rectangle();
			int rowY = 30 + ((i - 1) * L) + ((i - 1) * 10);
			rectRow[i].setBounds(30, rowY, (L * 4) + (3 * 10), L);
		}
	}
	
	/**
	 * This rectangles are used only for click events
	 * There will be 4 rectangles for each central rows
	 * 
	 * return void
	 */
	public static void addColumnsRectangles()
	{
		for (int i = 1; i <= 4; i++) {
			rectColumn[i] = new Rectangle();
			int rowX = 30 + ((i - 1) * L) + ((i - 1) * 10);
			rectColumn[i].setBounds(rowX, 30, L, (L * 4) + (3 * 10));
		}
	}
	
	/**
	 * Trigger move effect for all boxes involved in row or column move
	 * 
	 * @param type The type of line (column or row)
	 * @param key The key of the column or row involved
	 * @param direction
	 * 
	 * return void
	 */
	public static void moveLine(String type, int key, String direction)
	{
		// Because first and last lines are not usable we will increment the key 
		// to match only the middle lines
		key++;
		
		if (direction == "up" || direction == "left") {
			for (int i = 1; i <= perLine; i++) {
				switch (direction) {
					case "up":
						moveBoxToTop(i, key);
					break;
					case "left":
						moveBoxToLeft(i);
					break;
				}
			}
		} else {
			for (int i = perLine; i >= 1; i--) {
				switch (direction) {
					case "right":
						moveBoxToRight(key, i);
					break;
					case "down":
						moveBoxToBottom(i, key);
					break;
				}
			}
		}
	}
	
	/**
	 * Move box to top
	 * @param boxKey
	 * @param direction
	 */
	public static void moveBoxToTop(int row, int col)
	{
		System.out.println("move to top");
		Box box = getBox(row, col);
		
		if (box.getRow() == 1) {
			box.setRow(0);
			return;
		}
		
		if (box.getRow() == 0) {
			int type = getBox(5, box.getColumn()).getType();
			box.setType(type == 1 ? 2 : 1);
		}
		
		if (box.getRow() == 0) {
			box.setRow(perLine);
		} else {
			box.setRow(box.getRow() - 1);
		}
		
		if (box.getRow() == 5) {
			moveBoxToTop(0, col);
		}
	}
	
	/**
	 * Move box to right
	 * @param boxKey
	 * @param direction
	 */
	public static void moveBoxToRight(int row, int col)
	{
		Box box = getBox(row, col);
		System.out.println("row = " + row + " -> col = " + col);
		if (box.getColumn() == perLine) {
			box.setColumn(0);
			return;
		}
		
		if (box.getColumn() == 0) {
			int type = getBox(box.getRow(), 2).getType();
			box.setType(type == 1 ? 2 : 1);
		}
		
		box.setColumn(box.getColumn() + 1);
		
		if (box.getColumn() == 2) {
			moveBoxToRight(row, 0);
		}
	}
	
	/**
	 * Move box to bottom
	 * @param boxKey
	 * @param direction
	 */
	public static void moveBoxToBottom(int row, int col)
	{
		Box box = getBox(row, col);
		
		if (box.getRow() == perLine) {
			box.setRow(0);
			return;
		}
		
		if (box.getRow() == 0) {
			int type = getBox(2, box.getColumn()).getType();
			box.setType(type == 1 ? 2 : 1);
		}
		
		box.setRow(box.getRow() + 1);
		
		if (box.getRow() == 2) {
			moveBoxToBottom(0, col);
		}
	}

	/**
	 * Move box to left
	 * @param boxKey
	 * @param direction
	 */
	public static void moveBoxToLeft(int boxKey)
	{
		Box box = boxes[boxKey];
		
		if (box.getColumn() == 1) {
			System.out.println("This will be deleted : " + box.getColumn());
			box.setColumn(perLine);
			box.setRow(box.getRow());
			box.setRandomType();
		} else {
			box.setColumn(box.getColumn() - 1);
		}
	}
	
	/**
	 * Get Box by row and column
	 * 
	 * @param row
	 * @param col
	 * @return Box
	 */
	public static Box getBox(int row, int col)
	{
		Box box = null;
		for (int i = 0; i < boxCount; i++) {
			if (boxes[i].getRow() == row && boxes[i].getColumn() == col)
			{
				box = boxes[i];
				break;
			}
		}
		
		return box;
	}
	
	
	
}
